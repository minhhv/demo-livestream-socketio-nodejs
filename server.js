const express = require('express')
const app = express()
const server = require('http').createServer(app)
const io = require('socket.io')(server)

app.use('/', express.static('public'))

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

let roomOwns = {}

io.on('connection', (socket) => {
  socket.on('join', (payload) => {
    if (payload.isStreamer === 1) {
      if (roomOwns[payload.roomId]) {
        io.to(socket.id).emit('alert_message', 'Your livestream is running...')
      } else {
        console.log(`Creating room ${payload.roomId} and emitting room_created socket event`)

        roomOwns[payload.roomId] = socket.id;

        socket.join(payload.roomId)
        socket.emit('room_created', socket.id)
      }
    } else {
      if (checkRoomId(socket.id, roomOwns[payload.roomId])) {
        console.log(`Joining room ${payload.roomId} and emitting room_joined socket event`)

        socket.join(payload.roomId)
        socket.emit('room_joined', payload.roomId)
      }
    }
  });

  socket.on('start_call', (roomId) => {
    if (checkRoomId(socket.id, roomOwns[roomId])) {
      if (!roomOwns[roomId]) {
        io.to(socket.id).emit('alert_message', 'The room is not available. Please wait for streamer to broadcast.')
        return
      }

      console.log(`Broadcasting start_call event to peers in room ${roomId}`)
      socket.to(roomOwns[roomId]).emit('start_call', socket.id)
    }
  });

  socket.on('webrtc_offer', (event) => {
    if (checkRoomId(socket.id, roomOwns[event.roomId])) {
      console.log(`Broadcasting webrtc_offer event to peers in room ${event.roomId}`)

      socket.to(event.viewerSocketId).emit('webrtc_offer', event.sdp)
    }
  });

  socket.on('webrtc_answer', (event) => {
    if (checkRoomId(socket.id, roomOwns[event.roomId])) {

      console.log(`Broadcasting webrtc_answer event to peers in room ${event.roomId}`)
      socket.to(roomOwns[event.roomId]).emit('webrtc_answer', event.sdp)
    }
  })

  socket.on('webrtc_ice_candidate', (event) => {
    if (checkRoomId(socket.id, roomOwns[event.roomId])) {
      if (socket.id !== roomOwns[event.roomId]) {
        socket.to(roomOwns[event.roomId]).emit('webrtc_ice_candidate', event);
      } else {
        socket.broadcast.to(event.roomId).emit('webrtc_ice_candidate', event)
      }
    }
  });

  socket.on('end_stream', (roomId) => {
    if (roomOwns[roomId]) {
      delete roomOwns[roomId];
      socket.broadcast.to(roomId).emit('end_stream');
    }
  });

  socket.on('leave_room', (roomId) => {
    socket.leave(roomId);
  });
});

const checkRoomId = (socketId, roomId) => {
  if (!roomId) {
    io.to(socketId).emit('alert_message', 'Stream is not already');

    return false;
  }

  return true;
}

const port = process.env.PORT || 5000
server.listen(port, () => {
  console.log(`Express server listening on port ${port}`)
})
