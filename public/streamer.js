const roomSelectionContainer = document.getElementById('room-selection-container')
const roomInput = document.getElementById('room-input')
const connectButton = document.getElementById('connect-button')
const endStreamButton = document.getElementById('end-stream-button');

const videoChatContainer = document.getElementById('video-chat-container')
const localVideoComponent = document.getElementById('local-video')

document.querySelectorAll('audio, video').forEach(item => {
  item.muted = true;
  item.pause();
});

const socket = io('/')

const mediaConstraints = {
  video: true,
}

const iceServers = {
  iceServers: [
    { urls: 'stun:stun.l.google.com:19302' },
    { urls: 'stun:stun1.l.google.com:19302' },
    { urls: 'stun:stun2.l.google.com:19302' },
    { urls: 'stun:stun3.l.google.com:19302' },
    { urls: 'stun:stun4.l.google.com:19302' },
  ],
};

let roomId;
let localStream;
let rtcPeerConnection;
let recorder;
let audioStream;
let selfSocketId;

connectButton.addEventListener('click', () => {
  joinRoom(roomInput.value)
  endStreamButton.style.display = 'block'
})

endStreamButton.addEventListener('click', () => {
  endStream()
})

const joinRoom = (room) => {
  if (room === '') {
    alert('Please type a room ID')
  } else {
    roomId = room
    socket.emit('join', {roomId: roomId, isStreamer: 1})
    showVideoConference()
  }
}

const endStream = () => {
  if (rtcPeerConnection) {
    rtcPeerConnection.close();
    rtcPeerConnection = null;
  }
  if (localStream && recorder) {
    recorder.stopRecording(async () => {
      localStream.getTracks().forEach(track => track.stop());
      localStream = null;

      const blob = recorder.getBlob();
      const formData = new FormData();
      var file = new File([blob], "recorded_video.mp4", {type: "video/webm", lastModified: Date.now()});

      formData.append('video', file);
      formData.append('title', 'minh test video livestream');

      const uploadUrl = "https://youpl-dev-api.bappartners.com/api/v1/my-channel/videos/upload";
      const authToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyMjMiLCJqdGkiOiI2N2ExMGY5Zjg1MDA1NTIzOTg4Y2FmZjg2OTk3OTQxYjBlYTkwZGY5ZTc4MDU0OTA2OWU5OTJlNzI0YjA0NTBiMzRmYWMyYTQxODViZDdlNiIsImlhdCI6MTY5MDI4NDg0My43NTY5NjUsIm5iZiI6MTY5MDI4NDg0My43NTY5NzEsImV4cCI6MTcyMTkwNzI0My43NDk3OTMsInN1YiI6IjIiLCJzY29wZXMiOltdfQ.BM84NnZNDZXBEnZarTspuC_3IDnlbbGhOcKPJfB5Ahaz1u8Ywf9BxHVxoA17h2xDOMhXZFK5cw6RV41XPsl-W6ymQhOgnsOKSvubDMsEIL7SsoZE4Tk2fNqBI0y8i7p4H4r7J20dqI2UGPLuHsR-Bt_lpo4gcZTIEkihZxmVgvAhtruwzjQqdJCinPChtxWCOVO_7iXi_IFeqzFuCdBnEggGPsXtTCnkTzmyHRqNebX3dTkRZDCy56gXBUDU5R-mKdfaI3Tn7eVtbHtIUG4lQ2EmT8y4mGYVQRdlVCSwvRxP_KdljQWkB375LfMaTDqyNK6CUbest79Gp7RyMqqX2EH0LJofDmXkbSIhET3fMn7r9uDwOrchlnO14subWzFwnwdSJ-fLxOPpq7obmINdoR9CZ0j_1F_zkWU-o_Oe2CgboC3YJoJEElq-PqOGHC2vHeQ7GYpys6A-_pvUqxuMPm9IaiF4VZjPAF0c8UwasA3i9PLWpb4H-XZByYsTbyI7BLJintE9bQgh06stXtG82Z6NiSbiEyKkRV0AcwASvUhSPKGt5kxs2d_vq2u188GenYG0QRtOWHpTOC7iEU8pTU-MNevkTMENZd5Hx0oW95AUnrFWVgd6IoRCyTN9ABubTQFDnIVMr9sPOYBUb985z7AQem49E-t7-jb_T4bf4qI";

      try {
          const response = await fetch(uploadUrl, {
              method: "POST",
              headers: {
                  Authorization: `Bearer ${authToken}`,
              },
              body: formData,
          });

          if (!response.ok) {
              throw new Error("Failed to upload video");
          }
          const responseData = await response.json();
          console.log("Video uploaded successfully:", responseData);
        } catch (error) {
            console.error("Error uploading video:", error.message);
        }
    });
  }

  videoChatContainer.style.display = 'none';
  roomSelectionContainer.style.display = 'block';

  socket.emit('end_stream', roomId);

  endStreamButton.style.display = 'none';
};

const showVideoConference = () => {
  roomSelectionContainer.style = 'display: none'
  videoChatContainer.style = 'display: block'
}

const setLocalStream = async (mediaConstraints) => {
  let stream
  try {
    screenStream = await navigator.mediaDevices.getDisplayMedia(mediaConstraints)
    audioStream = await navigator.mediaDevices.getUserMedia({ audio: true });
    const mergedStream = new MediaStream([...screenStream.getTracks(), ...audioStream.getTracks()]);

    recorder = new RecordRTC(mergedStream, {
      type: 'video'
  });

  recorder.startRecording();
  stream = mergedStream;

  } catch (error) {
    console.error('Could not get user media', error)
  }

  localStream = stream
  localVideoComponent.srcObject = stream
};

const sendIceCandidate = (event) => {
  if (event.candidate) {
    socket.emit('webrtc_ice_candidate', {
      roomId,
      label: event.sdpMLineIndex,
      candidate: event.candidate,
    })
  }
}

const addLocalTracks = (rtcPeerConnection) => {
  localStream.getTracks().forEach((track) => {
    rtcPeerConnection.addTrack(track, localStream)
  })
}

const createOffer = async (rtcPeerConnection, viewerSocketId) => {
  let sessionDescription
  try {
    sessionDescription = await rtcPeerConnection.createOffer()
    rtcPeerConnection.setLocalDescription(sessionDescription)
  } catch (error) {
    console.error(error)
  }

  socket.emit('webrtc_offer', {
    type: 'webrtc_offer',
    sdp: sessionDescription,
    viewerSocketId: viewerSocketId,
    roomId,
  })
};

socket.on('room_created', async (socketId) => {
  console.log('Socket event callback: room_created')
  selfSocketId = socketId

  await setLocalStream(mediaConstraints)
});

socket.on('start_call', async (viewerSocketId) => {
  console.log('Socket event callback: start_call')

  rtcPeerConnection = new RTCPeerConnection(iceServers)
  addLocalTracks(rtcPeerConnection)
  rtcPeerConnection.onicecandidate = sendIceCandidate
  await createOffer(rtcPeerConnection, viewerSocketId)
});

socket.on('webrtc_ice_candidate', (event) => {
  var candidate = new RTCIceCandidate({
    sdpMLineIndex: event.label,
    candidate: event.candidate,
  })
  rtcPeerConnection.addIceCandidate(candidate)
  console.log('Socket event callback: webrtc_ice_candidate')
})

socket.on('webrtc_answer', (event) => {
  console.log('Socket event callback: webrtc_answer')

  rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event))
})

socket.on('alert_message', message => {
  alert(message);
});