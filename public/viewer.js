// DOM elements.
const roomSelectionContainer = document.getElementById('room-selection-container')
const roomInput = document.getElementById('room-input')
const connectButton = document.getElementById('connect-button')

const videoChatContainer = document.getElementById('video-chat-container')
const localVideoComponent = document.getElementById('local-video')

const socket = io('/')

const mediaConstraints = {
  video: true,
}

// Free public STUN servers provided by Google.
const iceServers = {
  iceServers: [
    { urls: 'stun:stun.l.google.com:19302' },
    { urls: 'stun:stun1.l.google.com:19302' },
    { urls: 'stun:stun2.l.google.com:19302' },
    { urls: 'stun:stun3.l.google.com:19302' },
    { urls: 'stun:stun4.l.google.com:19302' },
  ],
};

let rtcPeerConnection
let localStream;
let roomId;
let ownRoomSocket = null;

// BUTTON LISTENER ============================================================
connectButton.addEventListener('click', () => {
  joinRoom(roomInput.value)
})

const joinRoom = (room) => {
  if (room === '') {
    alert('Please type a room ID')
  } else {
    roomId = room
    socket.emit('join', {roomId: roomId, isStreamer: 0})
    showVideoConference()
  }
};

const showVideoConference = () => {
  roomSelectionContainer.style = 'display: none'
  videoChatContainer.style = 'display: block'
};

const sendIceCandidate = (event) => {
  if (event.candidate) {
    socket.emit('webrtc_ice_candidate', {
      roomId,
      label: event.candidate.sdpMLineIndex,
      candidate: event.candidate.candidate,
    })
  }
}

const createAnswer = async (rtcPeerConnection) => {
  let sessionDescription
  try {
    sessionDescription = await rtcPeerConnection.createAnswer()
    rtcPeerConnection.setLocalDescription(sessionDescription)
  } catch (error) {
    console.error(error)
  }

  socket.emit('webrtc_answer', {
    type: 'webrtc_answer',
    sdp: sessionDescription,
    roomId: roomId,
  })
}

const setLocalStream = (event) => {
  localVideoComponent.srcObject = event.streams[0]
}

// SOCKET EVENT CALLBACKS =====================================================
socket.on('room_joined', async () => {
  console.log('Socket event callback: room_joined')

  socket.emit('start_call', roomId)
});

socket.on('alert_message', message => {
  alert(message);
});

socket.on('webrtc_offer', async (event) => {
  console.log('Socket event callback: webrtc_offer')

  rtcPeerConnection = new RTCPeerConnection(iceServers)

  rtcPeerConnection.ontrack = setLocalStream
  rtcPeerConnection.onicecandidate = sendIceCandidate
  rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event))

  await createAnswer(rtcPeerConnection)
})

socket.on('webrtc_ice_candidate', (event) => {
  console.log('Socket event callback: webrtc_ice_candidate')
  // ICE candidate configuration.
  var candidate = new RTCIceCandidate({
    sdpMLineIndex: event.candidate.sdpMLineIndex,
    candidate: event.candidate.candidate,
  })
  rtcPeerConnection.addIceCandidate(candidate)
})

socket.on('end_stream', () => { 
  localVideoComponent.srcObject = null

  alert('End streaming');

  socket.emit('leave_room', roomId)
});